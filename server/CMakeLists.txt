cmake_minimum_required(VERSION 3.5.1)
project(server C)

set(CMAKE_C_STANDARD 11)

include(FindPkgConfig)

pkg_check_modules(ARROW_GLIB REQUIRED arrow-glib)
add_library(arrow-glib INTERFACE IMPORTED)
target_include_directories(arrow-glib INTERFACE ${ARROW_GLIB_INCLUDE_DIRS})
target_link_libraries(arrow-glib INTERFACE ${ARROW_GLIB_LINK_LIBRARIES})

pkg_check_modules(GIO2 REQUIRED gio-2.0)
add_library(gio2 INTERFACE IMPORTED)
target_include_directories(gio2 INTERFACE ${GIO2_INCLUDE_DIRS})
target_link_libraries(gio2 INTERFACE ${GIO2_LINK_LIBRARIES})

add_executable(server server.c)
target_link_libraries(server arrow-glib gio2)
