#include <arrow-glib/arrow-glib.h>

#include <stdlib.h>

static GArrowRecordBatchBuilder *
create_record_batch_builder(GError **error)
{
  GArrowFloatDataType *float_data_type = garrow_float_data_type_new();
  GList *fields = NULL;
  fields = g_list_append(fields,
                         garrow_field_new("x_column",
                                          GARROW_DATA_TYPE(float_data_type)));
  fields = g_list_append(fields,
                         garrow_field_new("y_column",
                                          GARROW_DATA_TYPE(float_data_type)));
  fields = g_list_append(fields,
                         garrow_field_new("z_column",
                                          GARROW_DATA_TYPE(float_data_type)));
  g_object_unref(float_data_type);

  GArrowSchema *schema = garrow_schema_new(fields);

  return garrow_record_batch_builder_new(schema, error);
}

static GArrowRecordBatch *
build_record_batch(GArrowRecordBatchBuilder *builder, GError **error)
{
  GArrowFloatArrayBuilder *x_builder =
    GARROW_FLOAT_ARRAY_BUILDER(
      garrow_record_batch_builder_get_column_builder(builder, 0));
  GArrowFloatArrayBuilder *y_builder =
    GARROW_FLOAT_ARRAY_BUILDER(
      garrow_record_batch_builder_get_column_builder(builder, 1));
  GArrowFloatArrayBuilder *z_builder =
    GARROW_FLOAT_ARRAY_BUILDER(
      garrow_record_batch_builder_get_column_builder(builder, 2));

#define N_RECORDS 100
  gfloat data_x[N_RECORDS];
  gfloat data_y[N_RECORDS];
  gfloat data_z[N_RECORDS];
  gboolean data_valid_x[N_RECORDS];
  gboolean data_valid_y[N_RECORDS];
  gboolean data_valid_z[N_RECORDS];
  for (gsize i = 0; i < N_RECORDS; i++) {
    data_x[i] = i;
    data_y[i] = i;
    data_z[i] = i;
    data_valid_x[i] = TRUE;
    data_valid_y[i] = TRUE;
    data_valid_z[i] = TRUE;
  }

  if (!garrow_float_array_builder_append_values(x_builder,
                                                data_x,
                                                N_RECORDS,
                                                data_valid_x,
                                                N_RECORDS,
                                                error)) {
    return NULL;
  }
  if (!garrow_float_array_builder_append_values(y_builder,
                                                data_y,
                                                N_RECORDS,
                                                data_valid_y,
                                                N_RECORDS,
                                                error)) {
    return NULL;
  }
  if (!garrow_float_array_builder_append_values(z_builder,
                                                data_z,
                                                N_RECORDS,
                                                data_valid_z,
                                                N_RECORDS,
                                                error)) {
    return NULL;
  }
#undef N_RECORDS

  return garrow_record_batch_builder_flush(builder, error);
}

static void
send_record_batch(GSocketConnection *client,
                  GArrowRecordBatch *record_batch,
                  GError **error)
{
  GOutputStream *client_output =
    g_io_stream_get_output_stream(G_IO_STREAM(client));
  GOutputStream *buffered_client_output =
    g_buffered_output_stream_new(client_output);
  GArrowGIOOutputStream *output =
    garrow_gio_output_stream_new(buffered_client_output);
  GArrowSchema *schema = garrow_record_batch_get_schema(record_batch);

  GArrowRecordBatchStreamWriter *stream_writer =
    garrow_record_batch_stream_writer_new(GARROW_OUTPUT_STREAM(output),
                                          schema,
                                          error);
  if (!stream_writer) {
    goto exit;
  }

  GArrowRecordBatchWriter *writer = GARROW_RECORD_BATCH_WRITER(stream_writer);
  if (!garrow_record_batch_writer_write_record_batch(writer,
                                                     record_batch,
                                                     error)) {
    goto exit;
  }

  if (!garrow_record_batch_writer_close(writer, error)) {
    goto exit;
  }

exit:
  if (stream_writer) {
    g_object_unref(stream_writer);
  }
  g_object_unref(schema);
  g_object_unref(output);
  g_object_unref(buffered_client_output);
}

int
main(int argc, char **argv)
{
  int exit_code = EXIT_SUCCESS;
  const gchar *error_message = NULL;
  GError *error = NULL;
  GSocketListener *listener = NULL;
  GArrowRecordBatchBuilder *builder = NULL;

  listener = g_socket_listener_new();
  const guint port = 9002;
  g_socket_listener_add_inet_port(listener, port, NULL, &error);
  if (error) {
    error_message = "failed to add port";
    goto exit;
  }

  builder = create_record_batch_builder(&error);
  if (error) {
    error_message = "failed to build record batch builder";
    goto exit;
  }

  while (TRUE) {
    GSocketConnection *client =
      g_socket_listener_accept(listener, NULL, NULL, &error);
    if (!client) {
      error_message = "failed to accept";
      goto exit;
    }

    GArrowRecordBatch *record_batch = build_record_batch(builder, &error);
    if (!record_batch) {
      g_object_unref(client);
      error_message = "failed to build record batch";
      goto exit;
    }

    send_record_batch(client, record_batch, &error);
    g_object_unref(record_batch);
    g_object_unref(client);
    if (error) {
      error_message = "failed to send";
      goto exit;
    }
  }

exit:
  if (error) {
    g_print("%s: %s", error_message, error->message);
    g_error_free(error);
    exit_code = EXIT_FAILURE;
  }
  if (builder) {
    g_object_unref(builder);
  }
  if (listener) {
    g_object_unref(listener);
  }

  return exit_code;
}
